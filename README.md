# Ansible Role: Set up docker-compose


Set up a docker-compose environment on Ubuntu. 
## Usage

* Clone the project and its submodule(s)
```bash
git clone --recurse-submodules <this repo>
```
* Initiate the submodule(s) recursively - the right way
```bash
git submodule update --init --recursive
git submodule foreach --recursive git fetch
git submodule foreach --recursive git checkout master
```
* You're ready to go !

☝️ *git version 2.17.1*

## Variables:

* **`use_new_docker_environment`**:

  * Default : `true`
  * Description : ``



* **`docker_user`**:

  * Default : `alexis`
  * Description : `The user that will perform the docker commands.`



* **`set_up_docker_group_and_user`**:

  * Default : `true`
  * Description : `Create new docker group and user.`



* **`create_default_containers`**:

  * Default : `false`
  * Description : `Create default container (mainly used to test that it's working fine.)`



* **`default_container_name`**:

  * Default : `docker-ubuntu`
  * Description : `The default test container name.`



* **`default_container_image`**:

  * Default : `ubuntu`
  * Description : `The default test docker image.`



* **`default_container_command`**:

  * Default : `sleep 1d`
  * Description : `The default test container command to be executed.`


## Tags:

* `apt`


* `packages`


* `apt_key`


* `apt_repository`


* `pip`


* `docker`


* `system`

## License
Project under the
 [MIT](https://tldrlegal.com/license/mit-license)
 License.

## Appendix
### Working with git submodules

* Add a submodule
```bash
git submodule add <repo_url>
```

* Update the submodules
```bash
git submodule update --recursive
git submodule foreach --recursive git fetch
git submodule foreach --recursive git checkout master
```

* Remove a gitsubmodule in a proper way ([check out the mess over there](https://stackoverflow.com/questions/1260748/how-do-i-remove-a-submodule/36593218#36593218))
```bash
# Remove the submodule entry from .git/config
git submodule deinit -f path/to/submodule

# Remove the submodule directory from the superproject's .git/modules directory
rm -rf .git/modules/submodule_name

# Remove the entry in .gitmodules and remove the submodule directory located at path/to/submodule
git rm -f path/to/submodule
```
*git version 2.17.1*



## Author Information
This role  was created by: [alexis_renard](https://renardalexis.com)

👉 https://fr.linkedin.com/in/renardalexis


## Automatically generated
Documentation generated using ansible-autodoc : [https://gitlab.com/alexisrenard-utils/ansible/ansible-autodoc](https://gitlab.com/alexisrenard-utils/ansible/ansible-autodoc)